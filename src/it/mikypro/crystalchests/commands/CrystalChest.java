package it.mikypro.crystalchests.commands;

import it.mikypro.crystalchests.CrystalChests;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CrystalChest implements CommandExecutor {

    private FileConfiguration configuration = CrystalChests.getPlugin(CrystalChests.class).getConfig();

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String label, String[] args) {

        if (command.getName().equalsIgnoreCase("CrystalChest")) {

            if (commandSender instanceof Player) {

                String permission = configuration.getString("Command.permission");

                if (commandSender.hasPermission(permission)) {

                    if (args.length == 0) {

                        Inventory inventory = Bukkit.createInventory(
                                null,
                                configuration.getInt("Inventory.size"),
                                configuration.getString("Inventory.name").replace("&", "§")
                        );
                        ItemStack acceptStack = namedGlassPane((short)5, "§a§lConfirm");
                        ItemStack denyStack = namedGlassPane((short) 14, "§c§lCancel");
                        ItemStack panelStack = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 15);

                        inventory.setItem(configuration.getInt("Inventory.size") - 1, acceptStack);
                        for (int i=2; i<=8; i++) {
                            inventory.setItem(configuration.getInt("Inventory.size") - i, panelStack);
                        }
                        inventory.setItem(configuration.getInt("Inventory.size") - 9, denyStack);

                        ((Player)commandSender).openInventory(inventory);
                    } else {

                        commandSender.sendMessage("§5Command Syntax: /CrystalChest");
                    }
                } else {

                    commandSender.sendMessage("§cFor doing this command you need to have the permission:§4 " + permission);
                }
            } else {

                commandSender.sendMessage("§cOnly players can do this command");
            }

            return true;
        }

        return false;
    }

    private ItemStack namedGlassPane(short damage, String name)  {
        ItemStack stack = new ItemStack(Material.STAINED_GLASS_PANE, 1, damage);
        ItemMeta meta = stack.getItemMeta();

        meta.setDisplayName(name);
        stack.setItemMeta(meta);

        return stack;
    }
}
