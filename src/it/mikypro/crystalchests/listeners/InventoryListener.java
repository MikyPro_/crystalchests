package it.mikypro.crystalchests.listeners;

import it.mikypro.crystalchests.CrystalChests;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import java.io.File;
import java.io.IOException;
import java.util.Collections;
import java.util.Random;

public class InventoryListener implements Listener {

    private FileConfiguration configuration = CrystalChests.getPlugin(CrystalChests.class).getConfig();
    private int emptySlots = 0;
    private int itemNumber = 0;

    @EventHandler
    public void onInventoryClose(InventoryCloseEvent event) {

        if (event.getInventory().getTitle().equals(configuration.getString("Inventory.name").replace("&", "§"))) {

            for (int i=0; i<=configuration.getInt("Inventory.size")-10; i++) {

                if (event.getInventory().getItem(i) != null) {

                    Item item = event.getPlayer().getWorld().dropItem(event.getPlayer().getLocation(), event.getInventory().getItem(i));
                    item.setPickupDelay(0);
                }
            }
        }
    }

    @EventHandler
    public void onInventoryClick(InventoryClickEvent event) {

        if (event.getInventory().getName().equals(configuration.getString("Inventory.previewName").replace("&", "§"))) {

            if (event.getInventory().getItem(event.getInventory().getSize()-1).getItemMeta().getDisplayName().contains(":" + event.getWhoClicked().getName())) {

                event.setCancelled(true);
            }

            if (event.getInventory().getItem(event.getInventory().getSize()-1).getItemMeta().getDisplayName().contains(":")) {

                String[] numbers = event.getInventory().getItem(event.getInventory().getSize()-1).getItemMeta().getDisplayName().split(":");

                if (isInt(numbers[0])) {

                    int numbersNum = Integer.parseInt(numbers[0]);

                    if (numbersNum > 0 && numbersNum <= 1000000000 &&
                            event.getInventory().getItem(event.getInventory().getSize()-1).getItemMeta().getDisplayName().contains(numbers[0] + ":")) {

                        event.setCancelled(true);
                    }
                }
            }

            for (ItemStack item : event.getWhoClicked().getInventory().getContents()) {

                if (item != null && item.hasItemMeta() && item.getItemMeta().hasLore() &&
                        item.getItemMeta().getLore().get(0).startsWith(configuration.getString("Chest.tokenPrefix").replace("&", "§"))) {

                    event.setCancelled(true);
                }
            }
        }

        if (event.getInventory().getName().equals(configuration.getString("Inventory.name").replace("&", "§"))) {

            if (event.getCurrentItem().getType().equals(Material.SKULL_ITEM)) {

                event.setCancelled(true);
            }

            if (event.getCurrentItem().getType().equals(Material.STAINED_GLASS_PANE) && event.getCurrentItem().getDurability() == (short)15) {

                event.setCancelled(true);
            }

            if (event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getType().equals(Material.STAINED_GLASS_PANE)) {

                event.setCancelled(true);

                if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§c§lCancel")) {

                    event.getWhoClicked().closeInventory();
                    event.getWhoClicked().sendMessage(configuration.getString("Gui.cancelMessage").replace("&", "§"));
                } else if (event.getCurrentItem().getItemMeta().getDisplayName().equals("§a§lConfirm")) {

                    for (int i=0; i<=configuration.getInt("Inventory.size")-9; i++) {

                        if (event.getInventory().getItem(i) == null) {

                            emptySlots = emptySlots+1;
                        }
                    }

                    if (emptySlots == configuration.getInt("Inventory.size")-9) {

                        event.getWhoClicked().sendMessage(configuration.getString("Gui.noItemsStored").replace("&", "§"));
                        emptySlots = 0;
                    } else {

                        Random random = new Random();
                        int chestCodeNum = random.nextInt(1000000000);

                        Player player = (Player)event.getWhoClicked();
                        String chestCode = chestCodeNum + ":" + player.getName();

                        File chestFile = new File("plugins/CrystalChest/Chests/" + chestCode + ".yml");
                        FileConfiguration chestFileConfiguration = YamlConfiguration.loadConfiguration(chestFile);

                        ConfigurationSection itemSection = chestFileConfiguration.createSection("items");

                        for (int i=0; i<=configuration.getInt("Inventory.size")-10; i++) {

                            if (event.getInventory().getItem(i) != null) {

                                if (event.getInventory().getItem(i).getType() != Material.SKULL_ITEM) {

                                    itemNumber = itemNumber + 1;
                                    itemSection.set("item-" + itemNumber, event.getInventory().getItem(i));
                                } else {

                                    event.getWhoClicked().sendMessage(configuration.getString("Gui.cancelMessage").replace("&", "§"));
                                    player.closeInventory();
                                    return;
                                }
                            }
                        }

                        try {
                            chestFileConfiguration.save(chestFile);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short)SkullType.PLAYER.ordinal());
                        SkullMeta meta = (SkullMeta)item.getItemMeta();

                        meta.setOwner("MHF_Chest");
                        meta.setDisplayName(configuration.getString("Chest.name").replace("&", "§") +
                                configuration.getString("Chest.subname").replace("&", "§"));
                        meta.setLore(Collections.singletonList(configuration.getString("Chest.tokenPrefix").replace("&", "§") + chestCode));
                        item.setItemMeta(meta);

                        Item entityItem = player.getWorld().dropItem(player.getLocation(), item);
                        entityItem.setPickupDelay(0);
                        event.getWhoClicked().sendMessage(configuration.getString("Gui.confirmMessage").replace("&", "§"));
                        player.updateInventory();

                        itemNumber = 0;
                        emptySlots = 0;
                    }

                    for (int i=0; i<=configuration.getInt("Inventory.size")-10; i++) {

                        if (event.getInventory().getItem(i) != null) {

                            event.getInventory().removeItem(event.getInventory().getItem(i));
                        }
                    }

                    event.getWhoClicked().closeInventory();
                }
            }
        }
    }

    private boolean isInt(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException | NullPointerException e) {
            return false;
        }
        return true;
    }
}
