package it.mikypro.crystalchests.listeners;

import it.mikypro.crystalchests.CrystalChests;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.File;

public class InteractListener implements Listener {

    private FileConfiguration configuration = CrystalChests.getPlugin(CrystalChests.class).getConfig();
    private int itemNum = 0;

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event) {

        if (event.getPlayer().getItemInHand().getType().equals(Material.SKULL_ITEM) && event.getPlayer().getItemInHand().hasItemMeta()) {

            if (event.getPlayer().getItemInHand().getItemMeta().getDisplayName().equals(
                    configuration.getString("Chest.name").replace("&", "§") +
                            configuration.getString("Chest.subname").replace("&", "§"))) {

                String token = event.getPlayer().getItemInHand().getItemMeta().getLore().get(0).replace(configuration.getString("Chest.tokenPrefix").replace("&", "§"), "");
                File chestFile = new File("plugins/CrystalChest/Chests/" + token + ".yml");

                if (event.getAction().equals(Action.LEFT_CLICK_BLOCK) || event.getAction().equals(Action.LEFT_CLICK_AIR)) {

                    if (chestFile.exists()) {

                        FileConfiguration chestFileConfiguration = YamlConfiguration.loadConfiguration(chestFile);
                        Inventory inventory = Bukkit.createInventory(
                                null,
                                configuration.getInt("Inventory.size"),
                                configuration.getString("Inventory.previewName").replace("&", "§")
                        );
                        ItemStack tokenStack = new ItemStack(Material.EMERALD, 1);
                        ItemMeta tokenMeta = tokenStack.getItemMeta();

                        tokenMeta.setDisplayName(token);
                        tokenStack.setItemMeta(tokenMeta);
                        inventory.setItem(inventory.getSize() - 1, tokenStack);

                        ConfigurationSection section = chestFileConfiguration.getConfigurationSection("items");

                        for (int i = 0; i <= section.getKeys(false).size() - 1; i++) {

                            itemNum = itemNum + 1;
                            inventory.addItem(section.getItemStack("item-" + itemNum));
                        }
                        itemNum = 0;
                        event.getPlayer().openInventory(inventory);
                    } else {

                        event.getPlayer().sendMessage("§cThe token of this crystal chest does not exist in the database.");
                    }
                } else if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) || event.getAction().equals(Action.RIGHT_CLICK_AIR)) {

                    if (chestFile.exists()) {

                        FileConfiguration chestFileConfiguration = YamlConfiguration.loadConfiguration(chestFile);
                        ConfigurationSection section = chestFileConfiguration.getConfigurationSection("items");

                        for (int i = 0; i <= section.getKeys(false).size() - 1; i++) {

                            itemNum = itemNum + 1;
                            event.getPlayer().getWorld().dropItem(event.getPlayer().getLocation(), section.getItemStack("item-" + itemNum));
                        }
                        itemNum = 0;
                        if (!chestFile.delete()) Bukkit.getConsoleSender().sendMessage("§cError when deleting file " + chestFile.getPath());
                        event.getPlayer().getInventory().removeItem(event.getPlayer().getItemInHand());
                    } else {

                        event.getPlayer().sendMessage("§cThe token of this crystal chest does not exist in the database.");
                    }
                }

                event.setCancelled(true);
            }
        }
    }
}
