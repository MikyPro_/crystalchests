package it.mikypro.crystalchests;

import it.mikypro.crystalchests.commands.CrystalChest;
import it.mikypro.crystalchests.listeners.InteractListener;
import it.mikypro.crystalchests.listeners.InventoryListener;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

public class CrystalChests extends JavaPlugin {

    @Override
    public void onEnable() {

        if (!new File("plugins/CrystalChest/config.yml").exists()) {
            getConfig().options().copyDefaults(true);
            saveDefaultConfig();
        }

        getCommand("CrystalChest").setExecutor(new CrystalChest());
        getServer().getPluginManager().registerEvents(new InventoryListener(), this);
        getServer().getPluginManager().registerEvents(new InteractListener(), this);
    }
}
